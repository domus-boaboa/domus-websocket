package org.boaboa.websocket.receiver;

import java.io.IOException;

import org.boaboa.websocket.controller.PanelsController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ReportReceiver {
	
	private static Logger LOGGER = LoggerFactory.getLogger(ReportReceiver.class);

	public static final String RECEIVEMESSAGE = "convertMessage";
	
	@Autowired
	private PanelsController _panels;
	
	public void convertMessage(String message) throws IOException {
		LOGGER.info(message);
		_panels.sendReportStatus(message);
	}

}
