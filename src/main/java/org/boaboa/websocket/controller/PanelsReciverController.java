package org.boaboa.websocket.controller;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class PanelsReciverController {

	@Autowired
	private RabbitTemplate _rabbitTemplate;

	@Value("${runArduino.exchangeName}")
	private String exchange;

	@Value("${runArduino.routingKey}")
	private String routingKey;

	@Autowired
	private ObjectMapper _mapper;

	@Autowired
	private SimpMessagingTemplate template;

	@MessageMapping("/digitalUpdate")
	public void greeting(DigitalWrite message) throws JsonProcessingException {
		String writeValueAsString = _mapper.writeValueAsString(message);
		_rabbitTemplate.convertAndSend(exchange, routingKey, writeValueAsString);
		template.convertAndSend("/report/digitalUpdate", message);
	}

}
