package org.boaboa.websocket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class PanelsController {

	@Autowired
	private SimpMessagingTemplate template;

	public void sendReportStatus(String message) {
		template.convertAndSend("/report/panel", message);
	}

}
