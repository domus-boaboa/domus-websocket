package org.boaboa.websocket.controller;

public class DigitalWrite {

	private String pin;

	private boolean status;
	
	public DigitalWrite() {
		
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
